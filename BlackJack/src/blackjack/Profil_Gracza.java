/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Kubuniu
 */
public class Profil_Gracza extends JFrame {
    
    JButton bClose;
    BazaDanych baza;
    String [] Informacje;
    JLabel [] podpisy;
    public Profil_Gracza(String login)
    {
        setSize(600,338);
        setTitle("ProfilGracza");
        setResizable(false);
        setLayout(null);
        
         try {
             setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("indeks.jpg")))));
         } catch (IOException ex) {
             Logger.getLogger(Klient_chat.class.getName()).log(Level.SEVERE, null, ex);
         }
        int x=20,y=20;
        baza=new BazaDanych();
        Informacje=baza.pobierzInfo(login);
        
        podpisy=new JLabel [9];
        podpisy[0]=new JLabel("Login "+Informacje[0]);
        podpisy[1]=new JLabel("Imie "+Informacje[1]);
        podpisy[2]=new JLabel("Aktualny Wynik "+Informacje[2]);
        podpisy[3]=new JLabel("Najniższy wynik"+Informacje[3]);
        podpisy[4]=new JLabel("Najwyzszy wynik "+Informacje[4]);
        podpisy[5]=new JLabel("Ilosc Gier "+Informacje[5]);
        podpisy[6]=new JLabel("Ilosc Wygranych  "+Informacje[6]);
        int a,b;
        a=Integer.parseInt(Informacje[5]);
        b=Integer.parseInt(Informacje[6]);
        double p=0;
        if(a!=0)
        p=b/a*100;
        
        podpisy[7]=new JLabel("Procent Wygranych  "+p+"%");
     

        podpisy[8]=new JLabel("Zarejestrowano "+Informacje[7]);
        
        
       
        for(int i=0;i<8;i++)
        {
             podpisy[i].setForeground(Color.orange);
            podpisy[i].setBounds(x,y,200,20);
            y+=50;
            add(podpisy[i]);
            
            if(i==3)
            {
                y=20;
                x+=250;
            }
            
        }
        podpisy[8].setForeground(Color.orange);
        podpisy[8].setBounds(x-100,y,250,20);
        add(podpisy[8]);
        setVisible(true);
    
    }
    
}
