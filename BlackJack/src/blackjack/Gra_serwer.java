/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Kubuniu
 */
public class Gra_serwer extends JFrame implements ActionListener,ChangeListener,Runnable{
JButton bKarta1,bHold1;
   Gamer gracz1;
   int x,y;
   boolean isplayed=false;
   BazaDanych baza;
   ServerSocket server1;
   int liczba_graczy=-1;
    String suma="";
    ArrayList<Gamer> gracze;
   ArrayList<Socket> polaczenie;
    private ArrayList<Thread> clientListenThreads;
    private ArrayList<ObjectOutputStream>output;
    String login;
    int kwota;
    JSlider suwak;
Gra_serwer(String Login) 
{       
    clientListenThreads = new ArrayList<Thread>();
        polaczenie = new ArrayList<Socket>();
       output=new ArrayList<ObjectOutputStream>();
       gracze=new ArrayList<Gamer>();
    
        login=Login;
        setSize(800,500);
        setTitle("  .:  BlackJack - serwer  :.    ");

        setLayout(null);
        setLocation(375,200);
        setResizable(false);
   
  
    
             
      try {
                 setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("bla (2).jpg")))));
             } catch (IOException ex) {
                 JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
             }
      
      bKarta1=new JButton("Karta");
    bKarta1.setBounds(150,400,100,20);
    bKarta1.addActionListener(this);
    add(bKarta1);
    
     try {
            bHold1=new JButton(new ImageIcon(ImageIO.read(new File("stop.jpg"))));
        } catch (IOException ex) {
            
        }
    bHold1.setBounds(600,400,50,50);
    bHold1.addActionListener(this);
    add(bHold1);
    
    x=150;
    y=30;
    
   
    
    gracz1=new Gamer(150,300);
   
    
    baza=new BazaDanych();
    gracz1.score=baza.pobierzwynik(login);
    
   suwak=new JSlider(JSlider.VERTICAL,0,gracz1.score,0);  
   suwak.setBounds(650,100,100,300);
  suwak.setMajorTickSpacing(gracz1.score/10);
 
   suwak.setPaintTicks(true);
   suwak.setPaintLabels(true);
   suwak.addChangeListener(this);
                add(suwak);
    
    
    
    setVisible(true);
    setBackground(Color.GREEN);
    
    
    
    
}
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object z=e.getSource();
        
        if(bKarta1==z)
        {
            gracz1.losuj(getGraphics());
            if(gracz1.sprawdz()==true)
            {
                
                isplayed=true;
                wyslij();
                
            }
        
        }
        else if(bHold1==z)
        {
            
          bKarta1.setEnabled(false);
                 
                isplayed=true;
                wyslij();
           
            }
         
        
        }
        
        
        
        
        
    
   boolean sprawdzGraczy(int a,ArrayList<Gamer> gracze)
    {
        for(int i=0;i<gracze.size();i++)
        {
        if(a>gracze.get(i).wynik)
        {
           
        }
        else
        {
            if(gracze.get(i).wynik<21)
            {
                 JOptionPane.showMessageDialog(null,"Wygrywa Przeciwnik :C","PRZEGRANA",JOptionPane.INFORMATION_MESSAGE);
            return false;
            }
            
           
        }
        }
        JOptionPane.showMessageDialog(null,"Wygrywasz !!","Wygrana",JOptionPane.INFORMATION_MESSAGE);
        return true;
    }
         
    
    void uruchom() 
    {
    try {
        server1=new ServerSocket(1111);
       
        while(true)
        {
            Socket client=czekanie();
            polaczenie.add(client);
            output.add(ustawStrumien(client));
            setupClientListenThread(clientListenThreads.size(), client);
            
        }
    } catch (IOException ex) {
       JOptionPane.showMessageDialog(null,"Bład przy stawianiu serwera","Bład",JOptionPane.INFORMATION_MESSAGE);
             System.exit(1);
    }
        
    }
    
     private void setupClientListenThread(int index, Socket clientSocket) {
		ClientListener clientListener = new ClientListener(index, clientSocket);
		Thread clientThread = new Thread(clientListener);
		clientListenThreads.add(clientThread);
		clientThread.start();
	}

   public Socket czekanie()
    {
        Socket client=null;
        try {
           
            client=server1.accept();
   
        } catch (IOException ex) {
            Logger.getLogger(Serwer_chat.class.getName()).log(Level.SEVERE, null, ex);
        }
        return client;
    }

   
           private ObjectOutputStream ustawStrumien(Socket a)  {
       
    try {
        return  new ObjectOutputStream( a.getOutputStream());
    } catch (IOException ex) {
         JOptionPane.showMessageDialog(null,"Bład przy ustawianiu strumieni","Bład",JOptionPane.INFORMATION_MESSAGE);
             System.exit(1);
    }
    return null;
    }
           
           
           class ClientListener implements Runnable {
private int indexInLists;
private Socket clientSocket;
private ObjectInputStream input;

private String userName;

		
		public ClientListener(int index, Socket clientSocket) {
                   
			indexInLists = index;
			this.clientSocket = clientSocket;
			setupInStream();			
			// TODO: Give proper option to set name
			userName = "User " + index;
		}
		
		private void setupInStream() {
			try {
				input=new ObjectInputStream(clientSocket.getInputStream());
			} catch(IOException e) {
				System.err.println("Problem getting client incoming stream.");
				e.printStackTrace();
			}
		}
		
		public void run() {
			listenForAndForwardMsgs();
		}
		
		private void listenForAndForwardMsgs()
                {	
                String message=null;
                do
                 {
                    try {
                        
                        message=(String) input.readObject();
                       

                        sendMessage(message);
                        gracze.add(new Gamer(x,y));
                        y+=50;
                        odbierz(message,gracze.get(gracze.size()-1));
                        
                        
                    } catch (IOException ex) {
                        Logger.getLogger(ClientListener.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(ClientListener.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Gra_serwer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }while(!message.equals("END"));
                 
       
    try {
        
        input.close();
    } catch (IOException ex) {
        Logger.getLogger(ClientListener.class.getName()).log(Level.SEVERE, null, ex);
    }
      
    
				
				
			
		}
	}
           public void sendMessage(String msg) {
		
		for (int i=0;i<output.size();i++) {
                    try {
                        output.get(i).writeObject(msg);
                    } catch (IOException ex) {
                        System.out.println("nie udalo sie wyslac wiadomosci");
                        Logger.getLogger(Serwer_chat.class.getName()).log(Level.SEVERE, null, ex);
                    }
			liczba_graczy++;
			if(polaczenie.size()==liczba_graczy && isplayed==true)
                        {
                            sendMessage("Koniec");
            if(sprawdzGraczy(gracz1.wynik,gracze)==true)
            {
                baza.aktualizujwynik(login,gracz1.score+kwota);
                
                dispose();
                
            }
            else
            {
                baza.aktualizujwynik(login,gracz1.score-kwota);
                new Gra_serwer(gracz1.login);
                dispose();
            }
                        }
		}
	}
        
        public void wyslij()
        
        {
             String wynik= login+":"+gracz1.wynik+":";
            suma ="";
            for(int i=0;i<gracz1.karty.size();i++)
            {
                StringBuilder sb = new StringBuilder();
                 
                sb.append(gracz1.karty.get(i).getKey());
                sb.append(":");
                String czesc= sb.toString();
                
                suma+=czesc;
            }
            wynik=wynik+suma;
            sendMessage(wynik);
        }
        
          public void odbierz(String message, Gamer gracz) throws IOException, ClassNotFoundException, InterruptedException
        {
            String[] pom1= message.split(":");
            gracz.login=pom1[0];
            
            gracz.wynik=Integer.valueOf(pom1[1]);
            
            if(gracz.login.equals(gracz1.login) && gracz.wynik==gracz1.wynik)
            {
                System.out.println("dostałem swoje wyniki");
            }
            else
            {
            System.out.println("Odebrano");
           for(int i=2;i<pom1.length;i++)
            {
                Random generator;
                generator = new Random();
                 Cards dodaj=new Cards();
                for(int j=0;j<52;j++)
                {
                    if(gracz.bla.Talia[j].getKey().equals(pom1[i]))
                    {
                        
                        dodaj=gracz.bla.Talia[j];
                        System.out.println("bla");
                               
            gracz.karty.add(dodaj);
            
            rysuj(dodaj.getPicture(),getGraphics(),gracz.x,gracz.y);
            Thread.sleep(1000);
            gracz.x+=100;
            break;
                    }
                    
                }
             
           
            }
            }
        }
        

    private void rysuj(Image get, Graphics graphics,int x,int y) {
        graphics.drawImage(get,x,y,this);
                    

    }

    @Override
    public void stateChanged(ChangeEvent e) {
       kwota=suwak.getValue();
    }

    @Override
    public void run() {
        uruchom();
    }

    
}
