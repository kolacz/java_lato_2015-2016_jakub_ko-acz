/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class TabelaWynikow  {

    TabelaWynikow() {

        String napis= "";

        BazaDanych bazaDanych=new BazaDanych();
        napis=bazaDanych.listaWynikow();

        JFrame window = new JFrame("Tabela wyników");
        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        final JTextArea textArea = new JTextArea(10, 20);
        JScrollPane scroll = new JScrollPane(textArea,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        textArea.setText(napis);
        textArea.setLineWrap(true);
        textArea.setFont(new Font("Arial", Font.BOLD, 26));
        textArea.setWrapStyleWord(true);
        textArea.setBackground(Color.GREEN);
        textArea.setEditable(false);
        textArea.setForeground(Color.RED);
        window.add(scroll);
        window.setSize(500, 500);
        window.setResizable(false);
        window.setVisible(true);
        window.setLocationRelativeTo(null);
    }
}