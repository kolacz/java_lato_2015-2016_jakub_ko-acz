package blackjack;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.UnsupportedAudioFileException;
import javazoom.jl.decoder.JavaLayerException;


public class BlackJack extends JFrame implements ActionListener,MouseListener
{
String login;
    private PanelHasla haslo;
    private JButton bGraj,bZasady,bWyniki,bWyjscie,bProfil;
     Radio odtwarzacz;
     static String Ip=" ";
     int opcja;
    BlackJack() 
    {
         try {
             haslo=new PanelHasla(this);
             login=haslo.login;
             haslo.dispose();
             opcja = JOptionPane.showConfirmDialog(null, "Serwer(YES) CLIENT(NO)", "WYBOR",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
             if(opcja==JOptionPane.YES_OPTION)
             {
                 Serwer_chat ser=new Serwer_chat(login);
                 Thread t1=new Thread(ser);
                 t1.start();
             }
             else
             {
                 Ip=JOptionPane.showInputDialog(this,"Podaj adres IP servera","IP",JOptionPane.INFORMATION_MESSAGE);
                 Klient_chat klient =new Klient_chat(Ip,login);
                 Thread t2=new Thread(klient);
                 t2.start();
             }
             setSize(640,456);
             setTitle("  .:  BlackJack   :.    ");
             setLayout(null);
             setLocation(375,200);
             setResizable(false);
             setBackground(Color.BLACK);
             try {
                 setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("tlo.jpg")))));
             } catch (IOException ex) {
                 Logger.getLogger(Klient_chat.class.getName()).log(Level.SEVERE, null, ex);
             }
             bProfil=new JButton(new ImageIcon(ImageIO.read(new File("gracz1.jpg"))));
             bProfil.setBounds(500,0,79,79);
             bProfil.setBackground(Color.PINK);
             bProfil.addActionListener(this);
             bProfil.addMouseListener(this);
             add(bProfil);
             bGraj=new JButton();
             bGraj.setIcon(new ImageIcon(ImageIO.read(new File("bgraj1.jpg"))));
             bGraj.setBounds(275,75,100,40);
             bGraj.addActionListener(this);
             bGraj.addMouseListener(this);
             add(bGraj);
             bZasady=new JButton();
             bZasady.setIcon(new ImageIcon(ImageIO.read(new File("informacje1.png"))));
             bZasady.setBounds(275,150,100,40);
             bZasady.addActionListener(this);
             bZasady.addMouseListener(this);
             add(bZasady);
             bWyniki=new JButton();
             bWyniki.setIcon(new ImageIcon(ImageIO.read(new File("tabela1.jpg"))));
             bWyniki.setBounds(275,225,100,40);
             bWyniki.addMouseListener(this);
             bWyniki.addActionListener(this);
             add(bWyniki);
             bWyjscie=new JButton();
             bWyjscie.setIcon(new ImageIcon(ImageIO.read(new File("wyjscie1.jpg"))));
             bWyjscie.setBounds(275,300,100,40);
             bWyjscie.addMouseListener(this);
             bWyjscie.addActionListener(this);
             add(bWyjscie);
             setVisible(true);
             odtwarzacz=new Radio();
         } catch (IOException ex) {
             JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
         }
        


    }

    public static void main(String[] args) 
    {
        String host = "jdbc:mysql://localhost:3306/Black-Jack";
        String user = "root";
        String passwd = "madafaka1";
        try {
            Connection con = DriverManager.getConnection(host, user, passwd);
        } catch (SQLException err) {
            ;
            System.out.println(err.getMessage());
            
        }
        BlackJack okienko=new BlackJack();
        okienko.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
        
       

    }

  

    @Override
    public void actionPerformed(ActionEvent e)
    {
    Object z=e.getSource();

        if(z==bGraj)
        {
            if(opcja==JOptionPane.NO_OPTION)
            {
                setVisible(false);
                Gra_Klient game=new Gra_Klient(Ip,login);
                Thread t3=new Thread(game);
            t3.start();
                
            }
            else
            {
                try {
                    setVisible(false);
                    Gra_serwer game=new Gra_serwer(login);
                    Thread t4=new Thread(game);
                    t4.start();
                } catch (IOException ex) {
                    Logger.getLogger(BlackJack.class.getName()).log(Level.SEVERE, null, ex);
                }
               
                
            }
        }
            else if(z==bWyniki)
        {
            TabelaWynikow tabela=new TabelaWynikow();
        }
            else if(z==bZasady)
        {
            JOptionPane.showMessageDialog(null,"INFORMACJE O GRZE","informacje",JOptionPane.INFORMATION_MESSAGE);
        }
            else if(z==bProfil)
            {
                Profil_Gracza nowy=new Profil_Gracza(login);
            }
        else if(z==bWyjscie)
        {
            dispose();
        }
    }

    
   
    @Override
    public void mouseClicked(MouseEvent e) {
      
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
       Object z=e.getSource();
       if(z==bGraj)
       {
           try {
               bGraj.setIcon(new ImageIcon(ImageIO.read(new File("bgraj.jpg"))));
           } catch (IOException ex) {
              JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
           }

       }
       else if(z==bZasady)
       {
           try {
               bZasady.setIcon(new ImageIcon(ImageIO.read(new File("informacje.png"))));
           } catch (IOException ex) {
              JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
           }

       }
       else if(z==bWyniki)
       {
           try {
               bWyniki.setIcon(new ImageIcon(ImageIO.read(new File("tabela.jpg"))));
           } catch (IOException ex) {
               JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
           }

       }
       else if(z==bWyjscie)
       {
           try {
               bWyjscie.setIcon(new ImageIcon(ImageIO.read(new File("wyjscie.jpg"))));
           } catch (IOException ex) {
               JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
           }

       }
       else if(z==bProfil)
       {
           try {
               bProfil.setIcon(new ImageIcon(ImageIO.read(new File("gracz.jpg"))));
           } catch (IOException ex) {
              JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
           }

       }
    }

    @Override
    public void mouseExited(MouseEvent e) {
          Object z=e.getSource();
       if(z==bGraj)
       {
        
              try {
                  bGraj.setIcon(new ImageIcon(ImageIO.read(new File("bgraj1.jpg"))));
              } catch (IOException ex) {
                 JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
              }
          

       }
       else if(z==bZasady)
       {
           try {
               bZasady.setIcon(new ImageIcon(ImageIO.read(new File("informacje1.png"))));
           } catch (IOException ex) {
               JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
           }

       }
       else if(z==bWyniki)
       {
           try {
               bWyniki.setIcon(new ImageIcon(ImageIO.read(new File("tabela1.jpg"))));
           } catch (IOException ex) {
               JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
           }

       }
       else if(z==bWyjscie)
       {
           try {
               bWyjscie.setIcon(new ImageIcon(ImageIO.read(new File("wyjscie1.jpg"))));
           } catch (IOException ex) {
               JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
           }

       }
      else if(z==bProfil)
       {
           try {
               bProfil.setIcon(new ImageIcon(ImageIO.read(new File("gracz1.jpg"))));
           } catch (IOException ex) {
               JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
           }

       }  
       
    }
    }

   



