
package blackjack;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Klient_chat extends JFrame implements ActionListener, Runnable{
     JTextField userText;
    JTextArea chatText;
    String massege=" ";
    String serverIP;
    Socket polaczenie;
    ObjectOutputStream output;
    ObjectInputStream input;
    String Login;
    public Klient_chat(String Ip,String xLogin)
    {
        Login=xLogin;
        serverIP=Ip;
        setTitle("chat-klient");
        setLocation(0,250);
        setSize(400,400);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
         //try {
            // setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("tlochat.jpg")))));
         //} catch (IOException ex) {
         //    Logger.getLogger(Klient_chat.class.getName()).log(Level.SEVERE, null, ex);
        // }
        userText=new JTextField();
        userText.setBounds(25,25,200,25);
        add(userText);
        userText.addActionListener(this);
        chatText=new JTextArea();
        JScrollPane scroll=new JScrollPane(chatText);
        scroll.setBounds(25,75,250,200);
        add(scroll);
        setVisible(true);
               
    }

    @Override
    public void actionPerformed(ActionEvent e) {
         try {
             wyslij(Login+": "+userText.getText());
         } catch (IOException ex) {
             Logger.getLogger(Klient_chat.class.getName()).log(Level.SEVERE, null, ex);
         }
        userText.setText("");
       
    }
public void uruchom() throws IOException, ClassNotFoundException
{
    polaczenieSerwer();
    ustawStrumienie();
    chatting();
}
    @Override
    public void run() {
         try {
             uruchom();
         } catch (IOException ex) {
             Logger.getLogger(Klient_chat.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(Klient_chat.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    private void polaczenieSerwer() throws UnknownHostException, IOException 
    {
       chatText.append("Probuje sie połaczyc... \n");
       polaczenie=new Socket(InetAddress.getByName(serverIP),13579);
        chatText.append(" Polaczono z: "+polaczenie.getInetAddress().getHostName());
    }

     private void ustawStrumienie() throws IOException
    {
        
        output= new ObjectOutputStream(polaczenie.getOutputStream());
        output.flush();
        input= new ObjectInputStream(polaczenie.getInputStream());
        output.writeObject(Login);
        
    }
     private void chatting() throws IOException, ClassNotFoundException
     {  String wiadomosc=null;
         do
         {
             wiadomosc=(String) input.readObject();
             chatText.append("\n "+wiadomosc);
         }while(!wiadomosc.equals("END"));
      output.close();
      input.close();
      polaczenie.close();
      
     }
     
     public void wyslij(String Text) throws IOException
     {
         output.writeObject(Text);
         output.flush();
         
     }
}
