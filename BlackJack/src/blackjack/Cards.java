/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Kubuniu
 */
public class Cards {
    
   public String key;
   public int value;
   public Image picture;
   public boolean isUsed;
   
   
   public Cards()
   {
       value=0;
       picture=null;
   }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * @return the picture
     */
    public Image getPicture() {
        return picture;
    }

    /**
     * @param picture the picture to set
     */
    public void setPicture(Image picture) {
        this.picture = picture;
    }

    /**
     * @return the isUsed
     */
    public boolean isIsUsed() {
        return isUsed;
    }

    /**
     * @param isUsed the isUsed to set
     */
    public void setIsUsed(boolean isUsed) {
        this.isUsed = isUsed;
    }
     public void rysuj(Graphics g, int x, int y)       
    {
        
        g.drawImage(picture,x,y,null);
    }
    
}
