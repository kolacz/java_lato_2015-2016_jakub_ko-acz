package blackjack;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StrumienieZapisuBinarnego {

   public StrumienieZapisuBinarnego()
   {

   }

    public void zapiszDoPlikuNowegoGracza(Rejestracja user,String odczyt) 
    {
        DataOutputStream strumień = null;


        try {
            strumień = new DataOutputStream(new FileOutputStream("Rejestracja.bin"));
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku");
        }

        try {
           Date dzis=new Date();
            String tekst="Login: "+user.getLogin()+" Hasło: "+user.getPassw()+" Imie: "+user.getImie()+" Nazwisko:" + user.getNazwisko()+" Email:" +user.getEmail()+" Data Rejestracji:"+dzis;
            
            odczyt=odczyt+tekst;
            strumień.writeUTF(odczyt);

        } catch (IOException e) {
            System.out.println("Problem z metodami zapisywania binarnego!");
        }

        try {
            if (strumień != null)
                
                strumień.close();
        } catch (IOException e) {
            System.out.println("Błąd zamykania strumienia");
        }
    }
    public String odczytajDane(String filename)  {

        InputStream is = null;
        DataInputStream dis = null;
           String odczyt="";

           File plik=new File(filename);
           if(plik.exists()!=true)
           {
            try {
                plik.createNewFile();
            } catch (IOException ex) {
                
            }
           }
        try{
            // create file input stream
            is = new FileInputStream(filename);

            // create new data input stream
            dis = new DataInputStream(is);

            // available stream to be read
            int length = dis.available();

            // create buffer
            byte[] buf = new byte[length];

            // read the full data into the buffer
            dis.readFully(buf);

            // for each byte in the buffer
            for (byte b:buf)
            {
                // convert byte to char
                char c = (char)b;
                odczyt = odczyt + c;
                // prints character
               // System.out.print(c);
            }
        }catch(Exception e){
            // if any error occurs
            
        }finally{

            try {
                // releases all system resources from the streams
                if(is!=null)
                    is.close();
                if(dis!=null)
                    dis.close();
            } catch (IOException ex) {
                Logger.getLogger(StrumienieZapisuBinarnego.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
           odczyt = odczyt + "\n";
        return odczyt;
    }
public void ZapisLogow(String login,String odczyt)
{
     DataOutputStream strumień = null;


        try {
            strumień = new DataOutputStream(new FileOutputStream("Blackjack-logs.bin"));
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku");
        }

        try {
           Date dzis=new Date();
            String tekst=login+" Zalogowal sie "+dzis;
            
            odczyt=odczyt+tekst;
            strumień.writeUTF(odczyt);

        } catch (IOException e) {
            System.out.println("Problem z metodami zapisywania binarnego!");
        }

        try {
            if (strumień != null)
                
                strumień.close();
        } catch (IOException e) {
            System.out.println("Błąd zamykania strumienia");
        }
        
        
    
}
public void Zapis(String login,String odczyt)
{
     DataOutputStream strumień = null;


        try {
            strumień = new DataOutputStream(new FileOutputStream("LogIn.bin"));
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku");
        }

        try {
           Date dzis=new Date();
            String tekst="%"+login+"%";
            
            odczyt=odczyt+tekst;
            strumień.writeUTF(odczyt);

        } catch (IOException e) {
            System.out.println("Problem z metodami zapisywania binarnego!");
        }

        try {
            if (strumień != null)
                
                strumień.close();
        } catch (IOException e) {
            System.out.println("Błąd zamykania strumienia");
        }
}
void zapiskonwersacji(ArrayList<String> loginy ,String msg)
{   
    DataOutputStream strumień=null;
   String a=loginy.get(0);
   
for(int i=0;i<loginy.size();i++)
{
    a+=loginy.get(i);
}
 String odczyt="";
 odczyt=odczytajDane(a+".bin");
        try {
      strumień = new DataOutputStream(new FileOutputStream(a+".bin"));
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku");
            
        }
        
    
         try {
            strumień.writeUTF(odczyt+"\n"+msg);
        } catch (IOException e) {
            System.out.println("Problem z metodami zapisywania binarnego!");
        }

}
}
