package blackjack;


import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class BazaDanych{
    private Connection con = null;
    private Statement stat = null;
int licznik=0;
    BazaDanych(){
        connectDatabase();
    }

    public void connectDatabase() {
        try{
            String host = "jdbc:mysql://localhost:3306/black-jack";
            String user = "root";
            String passwd = "madafaka1";
            con = DriverManager.getConnection( host, user, passwd );
            stat = con.createStatement();
            
        }
        catch ( SQLException err ) {
            System.out.println( "nie udalo sie polaczyc z baza danych" );
            
            try {
                con.close();
                stat.close();
                
            } catch (SQLException ex) {
               
            }
            System.exit(1);
        }
    }

    public boolean loginUsers (String login, String passw) throws IOException{
        LinkedList<Rejestracja> users = selectUsers();
        boolean b = false;
        if(users.size() != 0){
            for(int i =  0; i<users.size(); i++){
                if((users.get(i).getLogin().equals(login)) & (users.get(i).getPassw().equals(passw))) b = true;
            }
        }
        if(b == false) JOptionPane.showMessageDialog(null, "Wrong login or password!", "ERROR", JOptionPane.ERROR_MESSAGE);
        if(b == true)
        {
            int wynik=pobierzwynik(login);
            
            System.out.println("ZALOGOWANO");
            StrumienieZapisuBinarnego zapis1=new StrumienieZapisuBinarnego();
             String odczyt = zapis1.odczytajDane("Blackjack-logs.bin");
             
             zapis1.ZapisLogow(login,odczyt);
             StrumienieZapisuBinarnego zapis2=new StrumienieZapisuBinarnego();
             String odczyt1 = zapis2.odczytajDane("LogIn.bin");
             
             zapis1.Zapis(login,odczyt1);
             
             PreparedStatement prepStmt1=null;
           
            try {
                 ResultSet result;
                result = stat.executeQuery("SELECT Id_uzytkownika FROM `black-jack`.uzytkownicy WHERE login=\""+login+"\";");
                result.next();
                int id=result.getInt("Id_uzytkownika");
             prepStmt1=con.prepareStatement("UPDATE `black-jack`.profil SET ostatnie_logowanie=\""+new Date()+"\" WHERE Id_uzytkownika="+id+";");
             prepStmt1.executeUpdate();
             prepStmt1.close();
            } catch (SQLException ex) {
                System.out.println("nie udalo sie uaktualnic nowego logowania");
                System.exit(1);
            }
        }
        
        return b;
    }

    public boolean registerUser(String imie, String nazwisko, String email, String login, String haslo)  {
        LinkedList<Rejestracja> users = selectUsers();
        boolean b = false;
        //System.out.println("COS");
        if(users.size() != 0){
            for(int i=0; i<users.size(); i++){
                if(users.get(i).getLogin().equals(login)) {
                    b = false;
                    licznik=1;
                }
                else b = true;
                //System.out.println(licznik);

            }
        }
        else b = true;
        if(b == true && licznik==0){
             Rejestracja Gracz=new Rejestracja(imie,nazwisko,login,haslo,email);
             StrumienieZapisuBinarnego zapis=new StrumienieZapisuBinarnego();
             String odczyt=null;
             odczyt = zapis.odczytajDane("Rejestracja.bin");
             
             zapis.zapiszDoPlikuNowegoGracza(Gracz,odczyt);
             
             
             PreparedStatement prepStmt=null;
              
            try {
               
                Date dzis=new Date();
               
                prepStmt = con.prepareStatement("insert into `black-jack`.uzytkownicy values (NULL, ?, ?, ?, ?, ?, ?);");
                prepStmt.setString(1, imie);
                prepStmt.setString(2, nazwisko);
                prepStmt.setString(3, email);
                prepStmt.setString(4, login);
                prepStmt.setString(5, haslo);
                prepStmt.setString(6,  dzis.toString());
                
                prepStmt.executeUpdate();
                prepStmt.close();
                 dodajwynik(login);
            }
            catch (SQLException e) {
                
                System.err.println("Blad przy wstawianiu uzytkownika");
                System.exit(1);
                
                return false;
            }
            finally
            {
                if(prepStmt!=null)
                 try {
                     prepStmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(BazaDanych.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "User alredy exists!", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return b;
    }

    public LinkedList<Rejestracja> selectUsers() {
        LinkedList<Rejestracja> users = new LinkedList<Rejestracja>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM `black-jack`.uzytkownicy");
            String imie,nazwisko,email,login,haslo;
            while(result.next()) {
                login = result.getString("login");
                haslo = result.getString("haslo");
                email = result.getString("email");
                imie = result.getString("imie");
                nazwisko = result.getString("nazwisko");
                users.add(new Rejestracja(imie,nazwisko,login,haslo,email));
            }
            result.close();
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(null,"Bład przy pobieraniu uzytkownikow","Bład",JOptionPane.INFORMATION_MESSAGE);
            return null;
        }
        return users;
    }

    
    private boolean dodajwynik(String login)
    {
        PreparedStatement prepStmt=null;
        PreparedStatement prepStmt1=null;
            try {
                Date data=new Date();
                
                ResultSet result = stat.executeQuery("SELECT Id_uzytkownika FROM `black-jack`.uzytkownicy WHERE login=\""+login+"\";");
                result.next();
                int id=result.getInt("Id_uzytkownika");
                result.close();
                prepStmt = con.prepareStatement("insert into `black-jack`.wyniki values (?,?,?,?);");
                prepStmt.setInt(1, id);
                prepStmt.setInt(2,500);
                prepStmt.setInt(3,500);
                prepStmt.setInt(4,500);
                prepStmt.executeUpdate();
               prepStmt.close();
               
               prepStmt1= con.prepareStatement("insert into `black-jack`.profil values (?,?,0,0);");
               prepStmt1.setInt(1, id);
               prepStmt1.setString(2,data.toString() );
               prepStmt1.executeUpdate();
               
               prepStmt1.close();
               
               
              
               
                return true;
                
                
            }
            catch (SQLException e) {
                
                JOptionPane.showMessageDialog(null,"Bład przy wstawianiu uzytkownika,zarejestruj sie jeszcze raz","Bład",JOptionPane.INFORMATION_MESSAGE);
                new Rejestracja();
                
                return false;
            }
    }
    
    public int pobierzwynik(String login)
    {  
        ResultSet result=null;
        String x=null;
        int wynik=0;
       
        try {
            result= stat.executeQuery("select a.login ,b.aktualny from `black-jack`.uzytkownicy as a left join `black-jack`.wyniki as b on a.Id_uzytkownika=b.Id_uzytkownika where a.login=\""+login+"\";");
            if(result.next())
            {
            x=result.getString("aktualny");
            wynik=Integer.parseInt(x);
            }
       result.close();
        } catch (SQLException ex) {
           
            JOptionPane.showMessageDialog(null,"Bład przy odczycie wynikow","Bład",JOptionPane.INFORMATION_MESSAGE);
            System.exit(1);
        }
           
        
        
    return wynik;
    
    
}

    void aktualizujwynik(String login, int i) {
        try {
            
            ResultSet result = stat.executeQuery("SELECT Id_uzytkownika FROM `black-jack`.uzytkownicy WHERE login=\""+login+"\";");
                result.next();
                int id=result.getInt("Id_uzytkownika");
                result.close();
                 result = stat.executeQuery("SELECT minimalny,najwyzszy,aktualny FROM `black-jack`.wyniki WHERE Id_uzytkownika="+id+";");
                result.next();
                int a=result.getInt(1);
                int b=result.getInt(2);
                int c=result.getInt(3);
                result.close();
            String x=Integer.toString(i);
            PreparedStatement prepStmt=null;
            prepStmt = con.prepareStatement("UPDATE wyniki set aktualny="+i+" where Id_uzytkownika="+id+";");
            prepStmt.executeUpdate();
           
            if(c>i)
            {
                prepStmt = con.prepareStatement("UPDATE profil set ilosc_gier=ilosc_gier+1 where Id_uzytkownika="+id+";");
            prepStmt.executeUpdate();
            prepStmt = con.prepareStatement("UPDATE profil set ilosc_wygranych=ilosc_wygranych+1 where Id_uzytkownika="+id+";");
            prepStmt.executeUpdate();
            }
            else
            {
                 prepStmt = con.prepareStatement("UPDATE profil set ilosc_gier=ilosc_gier+1 where Id_uzytkownika="+id+";");
            prepStmt.executeUpdate();
                
            }
            
            
            
            if(a>i)
            {  
                prepStmt = con.prepareStatement("UPDATE wyniki set minimalny="+i+" where Id_uzytkownika="+id+";");
                prepStmt.executeUpdate();
            }
            else if(b<i)
            {
                prepStmt = con.prepareStatement("UPDATE wyniki set maksymalny="+i+" where Id_uzytkownika="+id+";");
                prepStmt.executeUpdate();
            }
            prepStmt.close();
            System.out.println("Zaktualizowano wynik");
        } catch (SQLException ex) {
            
            System.out.println("Nie pomyślnie zaktualizowano wynik "+ ex.getMessage());
        }
        
    }
    
    public String listaWynikow()
    {
String gotowyTekst="\n";

        
        try {
            ResultSet result2 = stat.executeQuery("SELECT * FROM wyniki");
            int ileGraczy=0;
            while(result2.next()) {
                ileGraczy++;
            }
            result2.close();
            int[] tablicaPunktow= new int[ileGraczy];
            String [] tablicaLoginow=new String[ileGraczy];

            ResultSet result = stat.executeQuery("SELECT a.login ,b.aktualny FROM `black-jack`.uzytkownicy as a left join `black-jack`.wyniki as b on a.Id_uzytkownika=b.Id_uzytkownika;");
            String login;
            int licznik=0,punkty;
            while(result.next()) {
                login = result.getString("Login");
                punkty = result.getInt("aktualny");
                tablicaLoginow[licznik]=login;
                tablicaPunktow[licznik]=punkty;
                licznik++;
            }

            
            int maxPunktow=0;
            for(int i=0; i<ileGraczy; i++)
            {
                if(maxPunktow<tablicaPunktow[i])
                {
                    maxPunktow=tablicaPunktow[i];
                }
            }

            for(int miejsceGracza=1; maxPunktow>=0;maxPunktow--)
            {
                for(int i=0; i<ileGraczy; i++)
                {
                    if(tablicaPunktow[i]==maxPunktow)
                    {
                        gotowyTekst=gotowyTekst+"   "+miejsceGracza+") "+tablicaLoginow[i]+" - "+tablicaPunktow[i]+" Punktów\n\n";
                        miejsceGracza++;
                    }
                }
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Bład przy wczytywaniu wynikow","Bład",JOptionPane.INFORMATION_MESSAGE);
            return null;
        }


        return gotowyTekst;        
    }
    

public String[] pobierzInfo(String login) {
    String[] dane=new String[8];
    
        try {
            ResultSet result = stat.executeQuery("SELECT a.login,a.imie,b.aktualny,b.minimalny,b.najwyzszy,c.ilosc_gier,c.ilosc_wygranych,a.data_rejestracji,c.ostatnie_logowanie FROM `black-jack`.uzytkownicy as a left join `black-jack`.wyniki as b on a.Id_uzytkownika=b.Id_uzytkownika left join `black-jack`.profil as c on a.Id_uzytkownika=c.Id_uzytkownika where a.login=\""+login+"\";");
        result.next();
        for(int i=0;i<8;i++)
        {
            dane[i]=result.getString(i+1);
        }
        result.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Bład przy odczytywaniu informacji o graczu","Bład",JOptionPane.INFORMATION_MESSAGE);
            return null;
        }


return dane;
}


}