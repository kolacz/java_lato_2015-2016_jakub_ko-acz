/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

class PanelHasla extends JDialog implements ActionListener
{
    JLabel lLogin,lPassword;
    JTextField tLogin;
    JPasswordField fPassword;
    JButton bZaloguj,bNowy;
    Rejestracja nowy;
    public String login,haslo;
            AutoComplete wpisywanieLoginu;
    
    public PanelHasla(JFrame owner)
    {
        super(owner,"Zaloguj sie",true);
        setSize(320,240);
        setLayout(null);
        setResizable(false);
        setLocation(500,300);


        lLogin=new JLabel("Login",JLabel.RIGHT);
        lLogin.setBounds(0,50,100,20);
        add(lLogin);

        //tLogin=new JTextField();
        //tLogin.setBounds(150,50,100,20);
        //=add(tLogin);

        lPassword=new JLabel("Haslo",JLabel.RIGHT);
        lPassword.setBounds(0,100,100,20);
        add(lPassword);

        fPassword=new JPasswordField();
        fPassword.setBounds(150,100,100,20);
        add(fPassword);

        bZaloguj=new JButton("Zaloguj");
        bZaloguj.setBounds(20,150,120,20);
        add(bZaloguj);
        bZaloguj.addActionListener(this);

        bNowy=new JButton("Zarejestruj sie");
        bNowy.setBounds(170,150,120,20);
        add(bNowy);
        bNowy.addActionListener(this);
        String tekst="";
        StrumienieZapisuBinarnego odczyt=new StrumienieZapisuBinarnego();
        tekst=odczyt.odczytajDane("LogIn.bin");

        int iloscLoginow=0;
        for(int i=0 ;i<tekst.length();i++)
        {
            if(tekst.charAt(i)=='%')
                iloscLoginow++;
        }
        iloscLoginow=iloscLoginow/2;

        String[] tablicaLoginow =new String[iloscLoginow];
        boolean czyToLogin=false;
        String pomocniczyDoLoginow="";
        for(int i=0, p=0; i<tekst.length();i++) {
            if (czyToLogin) {
                if (tekst.charAt(i) != '%')
                    pomocniczyDoLoginow = pomocniczyDoLoginow + tekst.charAt(i);
            }
            if (tekst.charAt(i) == '%') {
                if (czyToLogin == false) {
                    czyToLogin = true;
                } else if (czyToLogin == true) {
                    czyToLogin = false;
                    tablicaLoginow[p] = pomocniczyDoLoginow;
                    p++;
                    pomocniczyDoLoginow = "";
                }
            }
        }

         wpisywanieLoginu=new AutoComplete(tablicaLoginow);
        wpisywanieLoginu.main(tablicaLoginow,this);

        setVisible(true);
    }

    
    
    
    
    public void actionPerformed(ActionEvent e)
    {
        Object zrodlo=e.getSource();

        if(zrodlo==bNowy)
        {
            nowy=new Rejestracja(this);

        }
        else if(zrodlo==bZaloguj)
        {
            
            login=wpisywanieLoginu.getLogin();
            haslo=fPassword.getText();
            
            BazaDanych baza1=new BazaDanych();
            try {
                if(baza1.loginUsers(login,haslo)==true)
                {
                    
                    setVisible(false);
                }
                else
                {
                    tLogin.setText("");
                    fPassword.setText("");
                }
                //sprawdzanie czy podany login/haslo jest w bazie danych
            } catch (IOException ex) {
                Logger.getLogger(PanelHasla.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
   
}



