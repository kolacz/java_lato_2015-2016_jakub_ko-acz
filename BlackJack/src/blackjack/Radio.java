
package blackjack;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.beans.value.ObservableValue;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class Radio extends JFrame implements ActionListener,Runnable,ChangeListener {
    
    
   Clip clip;
    JButton bPlay,bStop;
    JButton bPauza,bNext,bPrevious;
    JTextArea tSpis;
    long clipTime;
    boolean isplayed=false;
    String[] sciezki;
    FloatControl gainControl;
     File plik;
    
     AudioInputStream audioInputStream;
     JSlider suwak;
    int numer=0,glosnosc=0;
    int min,max;
   
   public Radio() 
    {
        
             try {
                 setTitle("radio");
                 setSize(250,400);
                 setVisible(true);
                 setLocation(1000,0);
                 setLayout(null);
                    getContentPane().setBackground( Color.white );
                 bPlay=new JButton(new ImageIcon(ImageIO.read(new File("play.jpg"))));
                 bPlay.setBounds(100,20,50,50);
                 add(bPlay);
                 bPlay.addActionListener(this);
                 bPauza=new JButton(new ImageIcon(ImageIO.read(new File("zatrzymaj.png"))));
                 bPauza.setBounds(25,20,50,50);
                 add(bPauza);
                 bPauza.addActionListener(this);
                 bPrevious=new JButton(new ImageIcon(ImageIO.read(new File("poprzedni.png"))));
                 bPrevious.setBounds(50,75,50,50);
                 add(bPrevious);
                 bPrevious.addActionListener(this);
                 bNext=new JButton(new ImageIcon(ImageIO.read(new File("nastepny.png"))));
                 bNext.setBounds(125,75,50,50);
                 add(bNext);
                 bNext.addActionListener(this);
                 bStop=new JButton(new ImageIcon(ImageIO.read(new File("stop1.png"))));
                 bStop.setBounds(175,20,50,50);
                 add(bStop);
                 bStop.addActionListener(this);
                 tSpis=new JTextArea("");
                 JScrollPane scrollPane=new JScrollPane(tSpis);
                 scrollPane.setBounds(25,155,200,200);
                 add(scrollPane);
                 File f=new File("C:/Users/Kubuniu/Documents/NetBeansProjects/BlackJack/music");
                 sciezki=f.list();
                 for(int i=0;i<sciezki.length;i++)
                 {
                     if(i==numer)
                     {
                         
                         
                         
                         tSpis.append(i+1+". "+sciezki[i]+" |>"+"\n");
                     }
                     else
                     {
                         
                         
                         tSpis.append(i+1+". "+sciezki[i]+"\n");
                     }
                     sciezki[i]="music/"+sciezki[i];
                 }
                 plik=new File(sciezki[numer]);
                 try {
                     audioInputStream = AudioSystem.getAudioInputStream(plik);
                     clip = AudioSystem.getClip();
                     clip.open(audioInputStream);
                 } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                     JOptionPane.showMessageDialog(null,"Nie udało sie odtworzyc muzyki","Błąd",JOptionPane.ERROR_MESSAGE);
                     dispose();
                 }
                 gainControl =(FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                 gainControl.setValue(glosnosc);
                 min=(int) gainControl.getMinimum();
                 max=(int) gainControl.getMaximum();
                 suwak=new JSlider(min,max,0);
                 suwak.setBounds(25,125,150,20);
                 suwak.addChangeListener(this);
                 add(suwak);
                 clip.start();
             } catch (IOException ex) {
                 JOptionPane.showMessageDialog(null,"Bład wczytywaniu Radia","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
             }
           
            
            
       
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object z=e.getSource();
        if(z==bPlay)
        {
           clip.start();
             isplayed=true;
           
        }
        else if(z==bStop)
        {
            clip.stop();
            isplayed=false;
        }
        
        else if(z==bPauza)
        {
            if(isplayed==true)
            {
                isplayed=false;
                clipTime=clip.getMicrosecondPosition();
                clip.stop();
            }
            else 
            {
                isplayed=true;
                    clip.setMicrosecondPosition(clipTime);
                    clip.start();
            }
        }
        else if(bNext==z)
        {
            
                clip.stop();
                
                numer++;
                if(numer>sciezki.length-1)
                {
                    numer=0;
                }
                plik=new File(sciezki[numer]);
                
                   
                
               
            try {
                 audioInputStream = AudioSystem.getAudioInputStream(plik);
                clip = AudioSystem.getClip();
                clip.open(audioInputStream);
            } catch (LineUnavailableException | IOException  |UnsupportedAudioFileException ex) {
                JOptionPane.showMessageDialog(null,"Blad przy odtwarzaniu muzyki","Błąd",JOptionPane.ERROR_MESSAGE);
                dispose();
            }
                
                gainControl =(FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                gainControl.setValue(glosnosc); // Reduce volume by 10 decibels.
                clip.start();
                 tSpis.setText("");
                 for(int i=0;i<sciezki.length;i++)
            {
                 
                if(i==numer)
                {
                 
                    tSpis.append(i+1+". "+sciezki[i]+"    |>"+"\n");
                }
                else
                {
                    
                  
                  tSpis.append(i+1+". "+sciezki[i]+"\n");
                }
            }
            
            
            
        } else if(bPrevious==z)
        {
            
                clip.stop();
                numer--;
                if(numer<0)
                {
                    numer=sciezki.length-1;
                }
                plik=new File(sciezki[numer]);
                
            try {
                audioInputStream = AudioSystem.getAudioInputStream(plik);
                 clip = AudioSystem.getClip();
                 clip.open(audioInputStream);
            } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                JOptionPane.showMessageDialog(null,"Blad przy odtwarzaniu muzyki","Błąd",JOptionPane.ERROR_MESSAGE);
                dispose();
            }
               
                
                
                min=(int)gainControl.getMinimum();
                 max=(int)gainControl.getMaximum();
                 
                 
          
                clip.start();
                 tSpis.setText("");
                 for(int i=0;i<sciezki.length;i++)
            {
              
                if(i==numer)
                {
                    tSpis.append(i+1+". "+sciezki[i]+"    |>"+"\n");
                }
                else
                {
                  tSpis.append(i+1+". "+sciezki[i]+"\n");
                }
            }
           
        }
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void stateChanged(ChangeEvent e) 
    {
        glosnosc=suwak.getValue();
        clipTime=clip.getMicrosecondPosition();
        clip.stop();
        gainControl.setValue((float)glosnosc);
        clip.setMicrosecondPosition(clipTime);
        clip.start();
                
    }

 
   
}
