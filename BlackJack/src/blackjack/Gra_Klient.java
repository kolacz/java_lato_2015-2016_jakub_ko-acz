/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Kubuniu
 */
public class Gra_Klient extends JFrame implements ActionListener,ChangeListener,Runnable{
    JButton bKarta1,bHold1;
    Gamer gracz1;
     ArrayList<Gamer> gracze;
     int x,y;
    private String suma;
    String adresIP="";
    Socket polaczenie1;
    
    
    ObjectOutputStream output1;
    ObjectInputStream input1;
    private final BazaDanych baza;
    private final JSlider suwak;
    private int kwota;
    String Login;


    Gra_Klient(String Ip, String login) {
         adresIP=Ip;
         Login=login;
        setSize(800,500);
        setTitle("  .:  BlackJack - klient  :.    ");

        setLayout(null);
        setLocation(375,200);
        setResizable(false);
       
         try {
                 setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("bla (2).jpg")))));
             } catch (IOException ex) {
                 Logger.getLogger(Klient_chat.class.getName()).log(Level.SEVERE, null, ex);
             }
    bKarta1=new JButton("Karta");
    bKarta1.setBounds(150,400,100,20);
    bKarta1.addActionListener(this);
    add(bKarta1);
   
       
           
    
    
        try {
            bHold1=new JButton(new ImageIcon(ImageIO.read(new File("stop.jpg"))));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,"Bład wczytywaniu menu","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
        }
    bHold1.setBounds(600,400,50,50);
    bHold1.addActionListener(this);
    add(bHold1);
    
    
   
    
    gracz1=new Gamer(150,300);
    gracze=new ArrayList<Gamer>();
     x=150;
    y=30;
    
 
     
       
  
     baza=new BazaDanych();
    gracz1.score=baza.pobierzwynik(login);
    
   suwak=new JSlider(JSlider.VERTICAL,0,gracz1.score,0);  
   suwak.setBounds(650,100,100,300);
   suwak.setMajorTickSpacing(gracz1.score/10);
  
   suwak.setPaintTicks(true);
   suwak.setPaintLabels(true);
   suwak.addChangeListener(this);
   add(suwak);
    
    
    setVisible(true);
    setBackground(Color.GREEN);
    }

          
         

    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object z=e.getSource();
        
        if(bKarta1==z)
        {
            gracz1.losuj(getGraphics());
            if(gracz1.sprawdz()==true)
                wyslij();
        
        }
        else if(bHold1==z)
        {
            //sprawdzGraczy(gracz1.wynik,gracz2.wynik);
            //Gra_serwer nastepna=new Gra_serwer();
            //dispose();
            
            bKarta1.setEnabled(false);
            wyslij();
         
        
        }
        
        
        
        
        
    }
    
    
   boolean sprawdzGraczy(int a,ArrayList<Gamer> gracze)
    {
        for(int i=0;i<gracze.size();i++)
        {
        if(a>gracze.get(i).wynik)
        {
           
        }
        else
        {
            if(gracze.get(i).wynik<21)
            {
                 JOptionPane.showMessageDialog(null,"Wygrywa Przeciwnik :C","PRZEGRANA",JOptionPane.INFORMATION_MESSAGE);
            return false;
            }
            
           
        }
        }
        JOptionPane.showMessageDialog(null,"Wygrywasz !!","Wygrana",JOptionPane.INFORMATION_MESSAGE);
        return true;
    }
    

    private void polaczserwer()  {
       
        try {
            polaczenie1=new Socket(InetAddress.getByName(adresIP),1111);
            System.out.println("Połaczono z serwerem");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,"Nie mozna polaczyc sie z serwerem","Blad",JOptionPane.INFORMATION_MESSAGE);
        System.exit(1);
            
        }
     
    }

   
        private void ustawStrumienie() 
    {
        
        try {
            output1= new ObjectOutputStream(polaczenie1.getOutputStream());
            output1.flush();
            input1= new ObjectInputStream(polaczenie1.getInputStream());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,"Nie mozna polaczyc ustawic strumieni","Blad",JOptionPane.INFORMATION_MESSAGE);
        System.exit(1);
            
        }
    }
        
        public void wyslij() 
        {
            String wynik=Login+":"+gracz1.wynik+":";
           
            System.out.println("Wysłano Wynik");
            suma="";
            for(int i=0;i<gracz1.karty.size();i++)
            {
                StringBuilder sb = new StringBuilder();
                 
                sb.append(gracz1.karty.get(i).getKey());
                sb.append(":");
                String czesc= sb.toString();
                
                suma+=czesc;
            }
            wynik+=suma;
            System.out.println("Wyslano"+suma);
        try {
            output1.writeObject(wynik);
        } catch (IOException ex) {
           JOptionPane.showMessageDialog(null,"Nie mozna wyslac wiadomosc","Blad",JOptionPane.INFORMATION_MESSAGE);
        System.exit(1);
        }
        }
        
        
         public void odbierz(String message, Gamer gracz) throws IOException, ClassNotFoundException, InterruptedException
        {
            String[] pom1= message.split(":");
            gracz.login=pom1[0];
            
            gracz.wynik=Integer.valueOf(pom1[1]);
            if(gracz1.login.equals(gracz.login) && gracz1.wynik==gracz.wynik)
            {
                System.out.println("dostałem swoje wyniki");
            }
            else
            {
            System.out.println("Odebrano");
           for(int i=2;i<pom1.length;i++)
            {
                Random generator;
                generator = new Random();
                 Cards dodaj=new Cards();
                for(int j=0;j<52;j++)
                {
                    if(gracz.bla.Talia[j].getKey().equals(pom1[i]))
                    {
                        
                        dodaj=gracz.bla.Talia[j];
                        System.out.println("bla");
                               
            gracz.karty.add(dodaj);
            
            rysuj(dodaj.getPicture(),getGraphics(),gracz.x,gracz.y);
            Thread.sleep(1000);
            gracz.x+=100;
            break;
                    }
                    
                }
             
            }
            }
        }
        

    private void rysuj(Image get, Graphics graphics,int x,int y) {
        graphics.drawImage(get,x,y,this);
                    

    }

   
   /* public void run() {
        try {
            uruchom();
        } catch (IOException ex) {
            Logger.getLogger(Gra_Klient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
*/

    @Override
    public void stateChanged(ChangeEvent e) {
        kwota=suwak.getValue();
    }

    @Override
    public void run() {
        polaczserwer();
        ustawStrumienie();
       listenForAndForwardMsgs();
    }
    private void listenForAndForwardMsgs()
                {	
                String message=null;
                do
                 {
                   
                    try {
                        message=(String) input1.readObject();
                        if(message.equals("Koniec"))
                        {
                            if(sprawdzGraczy(gracz1.wynik,gracze)==true)
                            {
                                baza.aktualizujwynik(Login,gracz1.score+kwota);
                                 new Gra_Klient(adresIP,gracz1.login);
                                 dispose();
                            }
                            else
                            {
                                baza.aktualizujwynik(Login,gracz1.score-kwota);
                                 new Gra_Klient(adresIP,gracz1.login);
                                dispose();
                            }
                        }
                        else
                        {
                            gracze.add(new Gamer(x,y));
                            y+=50;
                            try {
                                odbierz(message,gracze.get(gracze.size()-1));
                            } catch (IOException ex) {
                                Logger.getLogger(Gra_Klient.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (ClassNotFoundException ex) {
                                Logger.getLogger(Gra_Klient.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(Gra_Klient.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(Gra_Klient.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Gra_Klient.class.getName()).log(Level.SEVERE, null, ex);
                    }
           
                       
                   
                }while(!message.equals("END"));
                 
       
    try {
        
        input1.close();
    } catch (IOException ex) {
        Logger.getLogger(Gra_Klient.class.getName()).log(Level.SEVERE, null, ex);
    }
      
    
				
				
			
		}
	}


    

