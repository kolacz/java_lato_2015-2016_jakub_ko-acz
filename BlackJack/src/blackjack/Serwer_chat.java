package blackjack;

import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Serwer_chat extends JFrame implements ActionListener,Runnable{
    JTextField userText;
    JTextArea chatText;
    ServerSocket serwer;
    String Login;
    ArrayList<Socket> polaczenie;
    private ArrayList<Thread> clientListenThreads;
    private ArrayList<ObjectOutputStream>output;
    private final ArrayList<String> loginy;
    StrumienieZapisuBinarnego strumien;
    public Serwer_chat(String xLogin)
    {
        strumien=new StrumienieZapisuBinarnego();
        loginy=new ArrayList<String>();
        clientListenThreads = new ArrayList<Thread>();
        polaczenie = new ArrayList<Socket>();
       output=new ArrayList<ObjectOutputStream>();
        setTitle("chat-serwer");
        Login=xLogin;
        setLocation(0,250);
        setSize(400,400);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        //try {
         //    setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("tlochat.jpg")))));
        // } catch (IOException ex) {
       //      Logger.getLogger(Klient_chat.class.getName()).log(Level.SEVERE, null, ex);
       //  }
        userText=new JTextField();
        userText.setBounds(25,25,200,25);
        add(userText);
        userText.addActionListener(this);
        chatText=new JTextArea();
        JScrollPane scroll=new JScrollPane(chatText);
        scroll.setBounds(25,75,250,200);
        add(scroll);
        setVisible(true);
               
    }
    public void uruchom() 
    {
        try {
            serwer=new ServerSocket(13579);
            while(true)
            {
               Socket client=czekanie();
               polaczenie.add(client);
               output.add(ustawStrumien(client));
               setupClientListenThread(clientListenThreads.size(), client);
            }
        } catch (IOException ex) {
             JOptionPane.showMessageDialog(null,"Bład tworzenia serwera","Bład",JOptionPane.INFORMATION_MESSAGE);
             dispose();
        }
        
    }
    public Socket czekanie()
    {
        Socket client=null;
        try {
            chatText.append("Czekanie na polaczenie uzytkownika...  \n");
            client=serwer.accept();
            chatText.append("Nowy uzytkownik!! o IP "+ client.getInetAddress().getHostName());
            
        } catch (IOException ex) {
             JOptionPane.showMessageDialog(null,"Bład podlaczenia klienta","Bład",JOptionPane.INFORMATION_MESSAGE);
             return null;
        }
        return client;
    }
    
    public void actionPerformed(ActionEvent e) {
        
        sendMessage(Login+": "+userText.getText());
        chatText.append(Login+": "+userText.getText()+"\n");
        userText.setText("");
    } 

    private void setupClientListenThread(int index, Socket clientSocket) {
		ClientListener clientListener = new ClientListener(index, clientSocket);
		Thread clientThread = new Thread(clientListener);
		clientListenThreads.add(clientThread);
		clientThread.start();
	}
      

    

    @Override
    public void run() {
        uruchom();
    }

    private ObjectOutputStream ustawStrumien(Socket a) throws IOException {
       
        return  new ObjectOutputStream( a.getOutputStream());
    }
    
    
    

class ClientListener implements Runnable {
    
private int indexInLists;
private Socket clientSocket;
private ObjectInputStream input;

private String userName;

		
		public ClientListener(int index, Socket clientSocket) {
                   
			indexInLists = index;
			this.clientSocket = clientSocket;
			setupInStream();			
			// TODO: Give proper option to set name
			userName = "User " + index;
		}
		
		private void setupInStream() {
			try {
				input=new ObjectInputStream(clientSocket.getInputStream());
			} catch(IOException e) {
				System.err.println("Problem getting client incoming stream.");
				e.printStackTrace();
			}
		}
		
		public void run() {
			listenForAndForwardMsgs();
		}
		
		private void listenForAndForwardMsgs()
                {	
                String message=null;
                do
                 {
                    try {
                        message=(String) input.readObject();
                      
                    } catch (IOException ex) {
                        Logger.getLogger(ClientListener.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(ClientListener.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    String [] loginys=message.split(":");
                        if(loginys.length==1)
                        {
                        
                            boolean ty=true;
                            for(int i=0;i<loginy.size();i++)
                            {
                                
                            if(loginy.get(i).equals(loginys[0]))  
                                ty=false;
                            }
                            if(ty==true)
                            loginy.add(loginys[0]);
                   }
                        else
                        {
                            sendMessage(message);
                        chatText.append(message+"\n");
                        }
                         
                        
                       
                        
                
                }while(!message.equals("END"));
                 
               
              
                 
       
    try {
        
        input.close();
    } catch (IOException ex) {
        Logger.getLogger(ClientListener.class.getName()).log(Level.SEVERE, null, ex);
    }
      
    
				
				
			
		}
	}
	public void sendMessage(String msg) {
		
		for (int i=0;i<output.size();i++) {
                    try {
                        output.get(i).writeObject(msg);
                    } catch (IOException ex) {
                        System.out.println("nie udalo sie wyslac wiadomosci");
                        Logger.getLogger(Serwer_chat.class.getName()).log(Level.SEVERE, null, ex);
                    }
			
			
		}
                strumien.zapiskonwersacji(loginy, msg);
	}
}
	
